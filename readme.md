# Just Another Chat Application: Backend

**NB!** This project will depend on [the frontend project](https://gitlab.com/just-another-chat-application/frontend)

See the [Design Document](./doc/design-document.md) for project overview until developed further.
