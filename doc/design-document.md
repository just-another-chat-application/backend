# Just Another Chat Application: Backend

This is the design document for the backend application for [Just Another Chat](https://gitlab.com/just-another-chat-application).

## Project Description

This will be a server using websocket for real time routing and short term storage of private messages between users while logging the absolute minimum required.

Message bodies should be encrypted client side to minimize trust in server with every user's public key being part of public user data.

## MVP Goals

Should implement all features described in this document.

While shortcuts are perfectly acceptable for the MVP of this project, it should aim to minimize available insights from compromising the server. The consequences of a data breach is a primary concern.

Familiarize myself further with web sockets and realtime communication.

## Features

### Authentication

Users must be able to authenticate with a username and password. This should result in a token for future authentication (probably JWT).

### Message Sending

Users must be able to send messages, through server, to other users.

### Message Storage

The server should store messages for a limited time to ensure a reasonable chance of delivery. Storage time must not exceed a week.

Message bodies must be encrypted client side and never be decryptable by the server.

### Find User

Users should be able to find other users with id or username to facilitate a basic contact list client side.

No blocking, server side contact list or mutual acceptance of being 'friends' are considered for MVP.

### Logging

Logging anything related to any user should be minimized. I am currently aiming to only log authentication usage.

## Milestones

- Defined interfaces for commands sent through websocket.
- Prototype can relay chats
- MVP can authenticate users
- MVP can save messages
- MVP has implemented all features promised in this document.

## Methodology

Solo agile development as a side project using my normal personal process for prototype to MVP.

## Technologies

Project will use Socket.io, TypeScript and Express for initial prototyping.
